"""Test bibtex parser module."""
from paperparser.bibtex_parser import bibtex


def test_arxiv_bibtex() -> None:
    """Can parse arxiv paper."""
    url = "https://arxiv.org/abs/1812.02900"
    bib = bibtex("arxiv", url)
    assert isinstance(bib, str)
    assert "Off-Policy Deep Reinforcement Learning without Exploration" in bib
    assert "Year" in bib


def test_nips_bibtex() -> None:
    """Can parse arxiv paper."""
    url = (
        "http://papers.nips.cc/paper/"
        "8296-multimodal-model-agnostic-meta-learning-via-task-aware-modulation"
    )
    bib = bibtex("nips", url)
    assert isinstance(bib, str)
    assert "Multimodal Model-Agnostic Meta-Learning" in bib


def test_doi_bibtex() -> None:
    """Can parse doi paper."""
    url = "https://doi.org/10.1016/j.engappai.2004.08.018"
    bib = bibtex("doi", url)
    assert isinstance(bib, str)
    assert "Application of reinforcement learning for agent-based" in bib


def test_acm_bibtex() -> None:
    """Parse ACM."""
    url = "https://dl.acm.org/doi/10.1016/j.engappai.2004.08.018"
    bib = bibtex("acm", url)
    assert isinstance(bib, str)
    assert "Application of reinforcement learning for agent-based" in bib


def test_failing_arxiv1() -> None:
    """Can parse arxiv paper."""
    url = "https://arxiv.org/abs/1506.02632"
    bib = bibtex("arxiv", url)
    assert isinstance(bib, str)
    assert "Cumulative Prospect Theory Meets" in bib


def test_failing_arxiv2() -> None:
    """Can parse arxiv paper."""
    url = "https://arxiv.org/abs/1706.10059"
    bib = bibtex("arxiv", url)
    assert isinstance(bib, str)
    assert "A Deep Reinforcement Learning Framework" in bib


def test_ieee() -> None:
    """Can parse ieee paper."""
    url = "https://ieeexplore.ieee.org/document/6502719"
    bib = bibtex("ieee", url)
    assert isinstance(bib, str)
    assert "Multiagent Reinforcement Learning for Integrated" in bib


def test_sciencedirect() -> None:
    """Can parse sciencedirect paper."""
    url = (
        "https://www.sciencedirect.com/science/article/pii/S0378778816300305?via%3Dihub"
    )
    bib = bibtex("sciencedirect", url)
    assert isinstance(bib, str)
    assert "Unsupervised energy prediction in a Smart Grid" in bib


def test_wiley() -> None:
    """Can parse wiley paper."""
    url = "https://onlinelibrary.wiley.com/doi/abs/10.1002/cpe.2864"
    bib = bibtex("wiley", url)
    assert isinstance(bib, str)
    assert "Applying reinforcement learning towards" in bib


def test_semanticscholar() -> None:
    """Can parse semanticsholar paper."""
    url = "https://www.semanticscholar.org/paper/\
        A-Learning-Based-Approach-for-the-Programming-of-Wang-Liang/\
            0345815682cc1de3ef5ad57d038694071b5b488e"
    bib = bibtex("semanticscholar", url)
    assert isinstance(bib, str)
    assert "A Learning-Based Approach for the Programming" in bib
